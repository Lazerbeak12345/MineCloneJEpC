--- Misc. Helper functions
mcljepc_core.helpers = {}

function mcljepc_core.helpers.atlas_id(tex, id)
	local x = id % 16
	local y = math.floor(id / 16)

	return tex..".png^[sheet:16x16:"..x..","..y
end

function mcljepc_core.helpers.terrain(id)
	return atlas_id("terrain", id)
end
