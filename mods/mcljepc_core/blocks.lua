
local blocks = {
	grass = {
		description = "Grass",
		-- color = "#8EB971",
		-- Color picked to be much lighter than above from MCL2, but still mostly green
		color = "#58ff3a",
		tiles = { "mcl_core_grass_block_top.png" }
		-- TODO make this a setting
		-- tiles = { mcljepc_core.helpers.terrain(0) }
	},
	cobblestone = {
		description = "Cobblestone",
		tiles = { "default_cobble.png" },
		-- tiles = { mcljepc_core.helpers.terrain(1) }
	}
}

for name, def in pairs(blocks) do
	if not def.groups then def.groups = {} end
	if not def.unbreakable then
		def.groups.instantly = 3
	end

	def.drop = ""

	def.stack_max = 1

	minetest.register_node("mcljepc_core:"..name, def)
end

minetest.register_craftitem("mcljepc_core:block", {
	inventory_image = "blank.png",
	wield_image = "blank.png",
	on_place = function(itemstack, user, pointed_thing)
		local nodename = "mcljepc_core:cobblestone"
		if pointed_thing.above.y == 0 then
			nodename = "mcljepc_core:grass"
		end

		-- Unlike set node, this actually checks against protection
		minetest.item_place(ItemStack(nodename), user, pointed_thing)
	end,
	on_drop = function(itemstack, dropper, pos)
		return itemstack
	end
})

