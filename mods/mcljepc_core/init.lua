mcljepc_core = {}
local modpath = minetest.get_modpath("mcljepc_core")
dofile(modpath .. "/helpers.lua")
dofile(modpath .. "/blocks.lua")

minetest.register_item(":", {
	type = "none",
	wield_image = "blank.png",
	range = 3, -- Got this value from a playtest
	tool_capabilities = {
		max_drop_level = 3,
		groupcaps = {
			instantly = {times = {[3] = 0}, uses = 0, maxlevel = 256}
		}
	}
})

minetest.set_timeofday(0.5) -- Midday TODO doesn't work?
minetest.settings:set('time_speed',0)
