local ymax = 21
local ymin = mcljepc_mapgen.depth * -1
local zmax = mcljepc_mapgen.size
local zmin = mcljepc_mapgen.size * -1
local xmax = mcljepc_mapgen.size
local xmin = mcljepc_mapgen.size * -1
local old_is_protected = minetest.is_protected
function minetest.is_protected(pos, name)
	return old_is_protected(pos, name) or not (
		    ymin <= pos.y and pos.y <= ymax
		and zmin <= pos.z and pos.z <= zmax
		and xmin <= pos.x and pos.x <= xmax
	)
end
