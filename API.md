# API

## Mod naming convention

Mods mods in MineCloneJEpC a simple naming convention: Mods with the prefix
“`mcljepc_`” are specific to MineCloneJEpC, although they may be based on an
existing standalone or a mod from a different MineClone fork. Mods which lack
this prefix are verbatim copies of a standalone mod. Some modifications may
still have been applied, but the APIs are held compatible.

## APIs
A lot of things are possible by using one of the APIs in the mods. Note that not
all APIs are documented yet, but it is planned. The following APIs should be
more or less stable but keep in mind that MineCloneJEpC is still unfinished. All
directory names are relative to `mods/`

### Planned APIs

* Event-based apis to ensure that specific startup code inside of mods can run
  in the correct order
