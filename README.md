# MineCloneJEpC

An unofficial Minecraft-like game for Minetest. Forked from MineClone 2 by Wuzzy
and merged with Minecraftn't Pre-classic by ROllerozxa
Developed by many people. Not developed or endorsed by Mojang AB.

Version: 0.1 (in development)

SPECIFIC VERSION OF MINECRAFT CLONED: `old_alpha rd-132211`

## Purpose and method

### Purpose

The primary purpose of this game is to improove MineClone 2 by researching the
development of Minecraft through implimenting each important release in-order.
As a side-effect, this will mean each release is a playable game on it's own -
but if this project ever catches up to MineClone 2, it will _not_ duplicate
code, and will avoid cloning the exact version that project is cloning.

### Method

- Make a game that clones the oldest Minecraft release.
- Make a modpack for each release following, that expands that game to become a
clone of the modpack's release.
- Where only absolutely necessary, fork the game to make a release. All future
modpacks are to be based on the new game. Such an example would be where making
a change to the `game.conf` file is the only option.

## Gameplay
You start in a flat world made entirely of cubes. You can explore the world and
dig almost every block in the world, and can use cobblestone to create new
structures.

Keeping true to the exact version of Minecraft that this is cloning, it's pretty
bare. There are only grass and cobblestone blocks. (TODO) Press the action key
to respawn.

### Gameplay summary

* Sandbox-style gameplay, no goals
* Use blocks to create great buildings, your imagination is the limit
* You can build almost anything for free (no cost) and without limit

## How to play (quick start)
### Getting started
- Mine with right click
- Place with left click
- Walk around

## Installation
This game requires [Minetest](http://minetest.net) to run (version 5.4.1 or
later). Older versions might work, but this is untested. So you need to install
Minetest first. Only stable versions of Minetest are officially supported.
There is no support for running MineCloneJEpC on development versions of
Minetest.

To install MineCloneJEpC (if you haven't already), move this directory into
the “games” directory of your Minetest data directory. Consult the help of
Minetest to learn more.

## Useful links
The MineCloneJEpC repository is hosted at Mesehub. To contribute or report issues, head there.

* Mesehub: <https://git.minetest.land/Lazerbeak12345/MineCloneJEpC>

Here's the Minecraft wiki page on the version of Minecraft that this is cloning.

<https://minecraft.fandom.com/wiki/Java_Edition_pre-Classic_rd-132211>

## Target

- Crucially, create a stable, moddable, free/libre clone of Minecraft
`old_alpha rd-132211` based on the Minetest engine with polished features.
 No other features will be added - though modifications to apis will be, if
 they facilitate making mods or modpacks to expand this game to clone a newer
 version of Minecraft.
- Optionally, create a performant experience that will run relatively
well on really low spec computers. Unfortunately, due to Minecraft's
mechanisms and Minetest engine's limitations along with a very small
playerbase on low spec computers, optimizations are hard to investigate.

## Completion status
This game is currently in **pre-alpha** stage.
It is playable, but has a few tweaks left.
Backwards-compability is not entirely guaranteed, updating your world might cause small bugs.

These things need figured out, and I don't know how to do them:

* In this edition of minecraft, a left-click places, and a right-click mines.
  (opposite of later versions, and of Minetest defaults)
* A way for modpacks to change values in `game.conf` (This will only effect how mods expanding this game function)
  * mapgen
  * gamemodes
  * etc.

The following main features are available (including features I must now remove):

* Blocks in the overworld
* Buidling blocks: Stone, Dirt
* Commands (TO BE REMOVED)
* Fully moddable (thanks to Minetest's powerful Lua API)
* And more! (It's a lot of work to remove - but not so much anymore thanks to ROllerozxa)

Technical differences from Minecraft:

* Height limit of ca. 31000 blocks (much higher than in Minecraft)
* Horizontal world size is ca. 62000×62000 blocks (much smaller than in Minecraft, but it is still very large)
* Still very incomplete and buggy
* Blocks, items and other features that shouldn't be there
* A few items have slightly different names to make them easier to distinguish
* Different textures (Pixel Perfection)
* Different sounds (various sources) (TO BE REMOVED)
* Different engine (Minetest)
* Different easter eggs

… and finally, MineCloneJEpC is free software (“free” as in “freedom”)!

## Other readme files

* `LICENSE.txt`: The GPLv3 license text
* `CONTRIBUTING.md`: Information for those who want to contribute
* `API.md`: For Minetest modders who want to mod this game
* `LEGAL.md`: Legal information
* `CREDITS.md`: List of everyone who contributed
