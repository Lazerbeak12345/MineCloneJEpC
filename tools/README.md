# MineClone 2 Tools
This directory is for tools and scripts for MineClone 2.
Currently, the only tool is Texture Converter.

## Texture Converter (EXPERIMENTAL)

To use the texture converter, copy the texture converter from upstream
MineClone 2 (where it is maintained) into this directory.

Follow their instructions to run the script - but in the context of this repo.
I've provided a modified `Conversion_Table.csv` to ensure the files go where
they should.

TODO
In the future I plan on supporting the `texture.png` texture system that older
versions of MineCraft used.
