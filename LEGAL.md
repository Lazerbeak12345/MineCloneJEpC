# Legal information
This is a fan game, not developed or endorsed by Mojang AB.

Copying is an act of love. Please copy and share! <3
Here's the detailed legalese for those who need it:

## License of source code
MineCloneJEpC (by kay27, EliasFleckenstein, Wuzzy, ROllerozxa,
Lazerbeak12345, davedevils and countless others) is an imitation
of Minecraft.

MineCloneJEpC is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License (in the LICENSE.txt file) for more
details.

In the mods you might find in the read-me or license
text files a different license. This counts as dual-licensing.
You can choose which license applies to you: Either the
license of MineClone 2 (GNU GPLv3) or the mod's license.

MineClone 2 is a direct continuation of the discontinued MineClone
project by davedevils. MineCloneJEpC is a fork of MineClone 2, which
was merged by Lazerbeak12345 with Minecraftn't Pre-classic by
ROllerozxa, a fork of Minecraftn't Classic by ROllerozxa, which is a
fork of MineClone 2.

Mod credits:
See `README.txt` or `README.md` in each mod directory for information about other authors.
For mods that do not have such a file, the license is the source code license
of MineCloneJEpC and the author is documented in the git history.

## License of media (textures and sounds)
No non-free licenses are used anywhere.

The textures, unless otherwise noted, are based on the Pixel Perfection resource pack for Minecraft 1.11,
authored by XSSheep. Most textures are verbatim copies, while some textures have been changed or redone
from scratch.
The glazed terracotta textures have been created by (MysticTempest)[https://github.com/MysticTempest].
Source: <https://www.planetminecraft.com/texture_pack/131pixel-perfection/>
License: [CC BY-SA 4.0](http://creativecommons.org/licenses/by-sa/4.0/)

The main menu images are release under: [CC0](https://creativecommons.org/publicdomain/zero/1.0/)

All other files, unless mentioned otherwise, fall under:
Creative Commons Attribution-ShareAlike 3.0 Unported (CC BY-SA 3.0)
http://creativecommons.org/licenses/by-sa/3.0/

See README.txt in each mod directory for detailed information about other authors.

Due to potential copyright problems with Minecraftn't Classic and
Pre-classic at one point containing assets copyrighted by Mojang
(which none of these projects are associated with), when manually
cherry-picking the code from Minecraftn't Pre-classic to be forked on
top of MCL2, I made sure to exclude anything that might be copyrighted
by Mojang.
